<?php
mb_http_input('UTF-8');
mb_http_output('UTF-8');
mb_internal_encoding('UTF-8');
setlocale(LC_ALL, 'ru_RU.UTF-8');
defined('YII_ENV') or define('YII_ENV', strpos($_SERVER['HTTP_HOST'] , 'test_rgkgroup') === false ? 'prod' : 'dev');
defined('YII_DEBUG') or define('YII_DEBUG', YII_ENV == 'dev' ? TRUE : FALSE);
error_reporting(YII_ENV == 'dev' ? E_ALL : 0);
ini_set('display_errors', YII_ENV == 'dev' ? 1 : 0);
$vendorPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor') . DIRECTORY_SEPARATOR;
$appPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'testApp') . DIRECTORY_SEPARATOR;;
require($vendorPath . 'autoload.php');
require($vendorPath . 'yiisoft/yii2/Yii.php');
$configApp = yii\helpers\ArrayHelper::merge(
    require($appPath . 'config/main.php'),
    require($appPath . 'config/prod.php'),
    require($appPath . 'config/local.php')
);
require($appPath. 'config/aliases.php');
$application = new yii\web\Application($configApp);
$application->run();