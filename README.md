# README #

# Установка приложения #
* Добавьте локальный хост-адрес "**test_rgkgroup.local**" к своему веб-серверу. Путь укажите до папки **.../htdocs/** включительно
* Задайте настройки базы данных в файле **testApp/config/db.php**

### Откройте консоль и перейдите в папку с файлом **composer.json** ###
* Установите плагин командой: 
```
#!php
composer global require "fxp/composer-asset-plugin:1.0.0-beta3"
```
* Установите необходимые зависимости для приложения: 
```
#!php
composer install --prefer-dist
```

### Откройте консоль и перейдите в папку **testApp** ###
* Выполните миграцию: 
```
#!php
php yii migrate --migrationPath=@books/migrations --interactive=0
```
* Выполните установку: 
```
#!php
php yii books/console/install
```

* Откройте браузер и перейдите по ссылке **http://test_rgkgroup.local/**
* Логин: den
* Пароль: BJtWU
* Установка завершена!