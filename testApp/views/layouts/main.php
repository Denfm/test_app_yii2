<?php
/* @var $content string */
use app\themes\dashboard\ThemeAsset;
use yii\helpers\Html;
ThemeAsset::register($this);
?>
<?php $this->beginPage(); ?>
<?php echo $this->render('//parts/head') ?>
<?php $this->beginBody(); ?>
<?php echo \yii\widgets\Breadcrumbs::widget(['homeLink' => ['label' => Yii::t('app', 'Главная'),
    'url' => Yii::$app->homeUrl], 'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
    <div class="l_menu">
        <ul class="nav">
            <li>
                <?php echo Html::a(Html::tag('i', '', ['class' => 'glyphicon glyphicon-th-large']) .
                    Yii::t('app', 'Каталог книг'), ['/books/base/index'])?>
            </li>
            <li>
                <?php echo Html::a(Html::tag('i', '', ['class' => 'glyphicon glyphicon-plus']) .
                    Yii::t('app', 'Добавить книгу'), ['/books/base/create'])?>
            </li>
        </ul>
    </div>
<div id="page-wrapper">
    <div class="m_content">
        <div class="content">
            <?php echo $content ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<?php $this->endBody(); ?>
<?php echo $this->render('//parts/foot') ?>
<?php $this->endPage(); ?>