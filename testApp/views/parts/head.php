<?php
use yii\helpers\Html;
use yii\bootstrap\BootstrapPluginAsset;
BootstrapPluginAsset::register($this);
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" prefix="og: http://ogp.me/ns#">
<head>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php echo Html::csrfMetaTags(); ?>
    <?php $this->head(); ?>
    <?php $this->registerMetaTag(['charset' => Yii::$app->charset, 'name'  => 'viewport',
        'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no']); ?>
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
<div id="wrapper">
    <div id="lbg"></div>
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">My Test</a>
        </div>
    </nav>
