<?php
namespace app\components;

use Yii, yii\helpers\Html;
use yii\base\Model;
use yii\caching\DbDependency;
use yii\helpers\FileHelper;

class AppHelper
{
    const CACHE_TIMEOUT = 7776000; // 90 дней
    const CACHE_IMAGE_DIR = 'cache';

    /**
     * @param string $arg
     * @param bool $inHtml
     */
    public static function debug($arg = '', $inHtml = false)
    {
        if (is_bool($arg))
            $arg = $arg ? 'true' : 'false';
        if (isset(Yii::$app->request->userAgent)) {
            if($inHtml)
                echo "\r\n" . '<!-- ' . "\r\n";
            else
                echo Html::beginTag('pre');
            print_r($arg);
            if($inHtml)
                echo "\r\n" . ' -->' . "\r\n";
            else {
                echo Html::endTag('pre');
                echo Html::tag('hr');
            }
        } else {
            print_r($arg);
            echo "\n";
        }
    }

    /**
     * @param $table
     * @param string $field
     * @param string $function
     * @return DbDependency
     */
    public static function DbDependency($table, $field = 'modified', $function = 'max')
    {
        $table = str_replace(['{', '}', '%'], [], $table);
        $cacheD = new DbDependency();
        $cacheD->sql = str_replace(
            ['TABLE', 'FUNCTION', 'FIELD'],
            [$table, $function, $field],
            'select FUNCTION(`FIELD`) from {{%TABLE}}'
        );
        return $cacheD;
    }

    /**
     * @param $errors
     * @param bool|false $asArray
     * @param string $sep
     * @return array|string
     */
    public static function modelErrors2String($errors, $asArray = false, $sep = "\r\n")
    {
        if($errors instanceof Model)
            $errors = $errors->getErrors();
        if(!is_array($errors))
            $errors = [];
        $stringErrors = [];
        foreach($errors as $k => $error)
            $stringErrors += $error;
        return $asArray ? $stringErrors : implode($sep, $stringErrors);
    }

    /**
     * @param array $paths
     * @param $file
     */
    public static function removeFileWithCache(Array $paths, $file)
    {
        $file_info = pathinfo($file);
        @unlink($paths['root'] . $file);
        if(count($file_info) == 4) {
            if(is_dir($paths['rootCache'])) {
                foreach(FileHelper::findFiles($paths['rootCache'], ['only' => ["*.{$file_info['extension']}"]]) as $file) {
                    AppHelper::debug($file);
                    if(preg_match("/^{$file_info['filename']}/", str_replace($paths['rootCache'], '', $file))) {
                        @unlink($file);
                    }
                }
            }
        }
    }

    /**
     * @param $date
     * @param string $format
     * @return string
     */
    public static function date2label($date, $format = 'Y-MM-dd')
    {
        $createDate = date('YMD', $date);
        if(date('YMD') == $createDate)
            return Yii::t('app', 'Сегодня');
        if(date('YMD', strtotime('-1 day')) == $createDate)
            return Yii::t('app', 'Вчера');
        return Yii::$app->formatter->asDatetime($date, $format);
    }
} 