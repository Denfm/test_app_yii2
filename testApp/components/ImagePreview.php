<?php
namespace app\components;

use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\imagine\Image;

class ImagePreview
{
    const QUALITY = 90;

    /**
     * @param $pic
     * @param array $previewSettings
     * @param array $paths
     * @return string
     * @throws Exception
     * @throws InvalidConfigException
     */
    public static function p($pic, Array $previewSettings, Array $paths)
    {
        $pathInfo = pathinfo($paths['root'] . $pic);
        if(!$pathInfo)
            throw new Exception(\Yii::t('app', 'Ошибка чтения файла.', ['f' => $paths['root'] . $pic]));

        $cacheFileName = self::fileName($pathInfo['filename'], $previewSettings) . '.' . $pathInfo['extension'];
        $path2fileCache = $paths['rootCache'] . DIRECTORY_SEPARATOR . $cacheFileName;

        // Файл в кеше уже есть
        if(file_exists($path2fileCache))
            return $paths['webCache'] . $cacheFileName;

        if(!is_dir($paths['rootCache'] ))
            FileHelper::createDirectory($paths['rootCache']);

        $path2file = $paths['root'] . $pic;

        if(!is_file($path2file))
            throw new Exception(\Yii::t('app', 'Исходный файл {f} не найден.', ['f' => $path2file]));

        $width = (isset($previewSettings['width']) && (int)$previewSettings['width']) ?
            (int)$previewSettings['width'] : 0;
        $height = (isset($previewSettings['height']) && (int)$previewSettings['height']) ?
            (int)$previewSettings['height'] : 0;
        $quality = (isset($previewSettings['quality']) && (int)$previewSettings['quality']) ?
            (int)$previewSettings['quality'] : self::QUALITY;

        if($width && $height && isset($previewSettings['crop']))
            Image::crop($path2file, $width, $height)->save($path2fileCache, ['quality' => $quality]);
        else
            Image::thumbnail($path2file, $width, $height)->save($path2fileCache, ['quality' => $quality]);

        return $paths['webCache'] . $cacheFileName;
    }

    /**
     * @param $filename
     * @param array $settings
     * @return string
     * @throws InvalidConfigException
     */
    protected static function fileName($filename, Array $settings)
    {
        $filename = [$filename];
        if(isset($settings['width']) && (int)$settings['width'])
            $filename[] = 'w' . (int)$settings['width'];
        if(isset($settings['height']) && (int)$settings['height'])
            $filename[] = 'h' . (int)$settings['height'];
        // Не указана высота и ширина (нужно чтобы было указан хотя бы 1 параметр)
        if(count($filename) == 1)
            throw new InvalidConfigException();
        // Для кропа обязательно надо, чтобы были все три параметра
        if(isset($settings['crop']) && count($filename) == 3)
            $filename[] = 'cr';
        return implode('_', $filename);
    }
}