<?php

namespace app\themes\dashboard;
use yii\base\BootstrapInterface;
use Yii;

/**
 * Themes bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        Yii::setAlias('theme', 'dashboard');
    }
}
