<?php
namespace app\themes\dashboard;
use yii\web\AssetBundle;
use yii\web\View;

class ThemeAssetPosHead extends AssetBundle
{
    public $sourcePath = '@app/themes/dashboard/assets';

    public $js = [
        'http://html5shiv.googlecode.com/svn/trunk/html5.js',
    ];

    public function init() {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }

    public $depends = [
        'yii\web\YiiAsset',
    ];
} 