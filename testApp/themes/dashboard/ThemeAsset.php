<?php

namespace app\themes\dashboard;
use yii\web\AssetBundle;
use yii\web\View;

class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/dashboard/assets';

    public $css = [
        'http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,cyrillic-ext'
    ];

    public $js = [

    ];

    public $depends = [
        'app\themes\dashboard\ThemeAssetPosHead'
    ];

    public function init() {
        $this->css[] = 'css/' . \Yii::$app->language . '.css';
        $this->jsOptions['position'] = View::POS_END;
        parent::init();
    }
}
