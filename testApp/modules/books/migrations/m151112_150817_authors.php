<?php

use yii\db\Schema;
use yii\db\Migration;
use \app\modules\books\models\Authors;

class m151112_150817_authors extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $table = Authors::tableName();

        $this->createTable($table, [
            'id' => Schema::TYPE_PK,
            'firstname' => Schema::TYPE_STRING . '(100) NOT NULL',
            'lastname' => Schema::TYPE_STRING . '(100) NOT NULL',
        ], $tableOptions);

        $this->createIndex('firstname', $table, 'firstname');
        $this->createIndex('lastname', $table, 'lastname');
    }

    public function safeDown()
    {
        $this->dropTable(Authors::tableName());
    }
}
