<?php

use yii\db\Schema;
use yii\db\Migration;
use \app\modules\books\models\Books;
use \app\modules\books\models\Authors;

class m151112_150842_books extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $table = Books::tableName();

        $this->createTable($table, [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'date_create' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0',
            'date_update' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0',
            'preview' => Schema::TYPE_STRING . '(100) NOT NULL',
            'date' => Schema::TYPE_DATE,
            'author_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0'
        ], $tableOptions);

        $this->createIndex('date_create', $table, 'date_create');
        $this->createIndex('date', $table, 'date');
        $this->createIndex('author_id', $table, 'author_id');
        $this->addForeignKey('FK_trk_aid', $table, 'author_id', Authors::tableName(), 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_trk_aid', Books::tableName());
        $this->dropTable(Books::tableName());
    }
}
