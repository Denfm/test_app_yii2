<?php

namespace app\modules\books\models\search;
use app\modules\books\models\Books;
use Yii, yii\data\ActiveDataProvider;

class BooksSearch extends Books
{
    public $date_start, $date_end;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['date_start', 'date_end'], 'date', 'format' => 'yyyy-MM-dd'],
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     * @return ActiveDataProvider DataProvider
     */
    public function search(Array $params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider(['query' => $query]);

        if(!($this->load($params, '')))
            return $dataProvider;

        $p = self::tableName();

        if(!empty($this->author_id))
            $query->andWhere($p . '.author_id = :author_id', ['author_id' => $this->author_id]);
        if(!empty($this->name))
            $query->andFilterWhere(['like', $p . '.name',  $this->name]);

        if(!empty($this->date_start) && !empty($this->date_end))
            $query->andFilterWhere(['between', $p . '.date',  $this->date_start, $this->date_end]);
        elseif(!empty($this->date_start) or !empty($this->date_end))
            $query->andWhere($p . '.date = :date', ['date' => empty($this->date_start) ? $this->date_end : $this->date_start]);

        return $dataProvider;
    }
}
