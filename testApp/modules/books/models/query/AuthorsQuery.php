<?php

namespace app\modules\books\models\query;
use app\modules\books\models\Authors;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Authors]].
 *
 * @see Actors
 */
class AuthorsQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Authors[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Authors|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}