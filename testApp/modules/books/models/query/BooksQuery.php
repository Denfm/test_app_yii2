<?php

namespace app\modules\books\models\query;
use app\modules\books\models\Books;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Books]].
 *
 * @see Catalog
 */
class BooksQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Books[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Books|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}