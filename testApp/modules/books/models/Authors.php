<?php

namespace app\modules\books\models;

use \app\modules\books\models\query\AuthorsQuery;
use Yii;
use yii\db\ActiveRecord;
use app\modules\books\Module;
use app\modules\books\traits\ModuleTrait;

/**
 * This is the model class for table "{{%authors}}".
 *
 * @property string $id
 * @property string $firstname
 * @property integer $lastname
 *
 * @property string $fullName
 * @property Books[] $books
 *
 */
class Authors extends ActiveRecord
{
    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%authors}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['firstname', 'lastname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAttributeLabel($attribute)
    {
        return Module::t('AUTHORS_ATTR_' . strtoupper($attribute));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogs()
    {
        return $this->hasMany(Books::className(), ['id' => 'author_id']);
    }

    /**
     * @inheritdoc
     * @return AuthorsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AuthorsQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
