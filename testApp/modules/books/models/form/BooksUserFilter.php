<?php
namespace app\modules\books\models\form;
use yii\base\Model;
use app\modules\books\Module;
use app\modules\books\traits\ModuleTrait;

class BooksUserFilter extends Model
{
    use ModuleTrait;

    public $author_id, $name, $date_start, $date_end;
    protected $params = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['date_start', 'date_end'], 'date', 'format' => 'yyyy-MM-dd'],
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->params = [];
        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        parent::afterValidate();
        if(!$this->hasErrors()) {
            $this->params = [
                'author_id' => $this->author_id,
                'name' => $this->name,
                'date_start' => $this->date_start,
                'date_end' => $this->date_end,
            ];
        }
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function getAttributeLabel($attribute)
    {
        return Module::t('BOOKS_ATTR_' . strtoupper($attribute));
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }
}