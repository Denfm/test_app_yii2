<?php

namespace app\modules\books\models;

use app\components\AppHelper;
use app\components\ImagePreview;
use app\modules\books\models\query\BooksQuery;
use app\modules\books\Module;
use app\modules\books\traits\ModuleTrait;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%books}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_create
 * @property integer $date_update
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 *
 * @property string $previewAsImage
 * @property string $previewSmall
 * @property string $updateDateLabel
 * @property string $createDateLabel
 * @property string $dateLabel
 * @property string $authorName
 * @property Authors $author
 *
 */
class Books extends ActiveRecord
{
    use ModuleTrait;

    protected static $filePath = ['preview' => 'books'];

    protected static $previewsSettings = [
        'preview' => ['grid' => ['width' => 120, 'height' => 90]]
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::className(), 'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update', 'value' => new Expression('unix_timestamp()')]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%books}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'author_id', 'date'], 'required'],
            [['author_id', 'date_create', 'date_update', 'author_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['preview'], 'file', 'extensions' => ['png', 'jpg', 'gif']]
        ];
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function getAttributeLabel($attribute)
    {
        return Module::t('BOOKS_ATTR_' . strtoupper($attribute));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }

    /**
     * @inheritdoc
     * @return BooksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BooksQuery(get_called_class());
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $paths = $this->filePaths('preview', false);
        AppHelper::removeFileWithCache($paths, $this->preview);
    }


    /**
     * @param $attribute
     * @param $section
     * @return bool|[]
     */
    public static function pws($attribute, $section)
    {
        if(isset(self::$previewsSettings[$attribute], self::$previewsSettings[$attribute][$section]))
            return self::$previewsSettings[$attribute][$section];
        return false;
    }

    /**
     * @param $attribute
     * @param bool|true $check
     * @return array
     * @throws \yii\base\Exception
     */
    public static function filePaths($attribute, $check = true)
    {
        if(!isset(self::$filePath[$attribute]))
            throw new InvalidParamException('Ключ {k} не найден в сво-ве {p}', ['k' => $attribute, 'p' => '$filePath']);

        $work_dir = self::$filePath[$attribute];
        $root = Yii::getAlias('@uploads_root') . DIRECTORY_SEPARATOR . $work_dir . DIRECTORY_SEPARATOR;
        $rootCache = $root . AppHelper::CACHE_IMAGE_DIR . DIRECTORY_SEPARATOR;
        $web = Url::base() . '/' . Yii::getAlias('uploads') . '/' . $work_dir . '/';
        $webCache = Url::base() . '/' . Yii::getAlias('uploads') . '/' . $work_dir . '/' .
            AppHelper::CACHE_IMAGE_DIR . '/';

        if($check) {
            if(!is_dir($root) or !is_dir($rootCache))
                FileHelper::createDirectory($rootCache);
        }

        return ['root' => $root, 'rootCache' => $rootCache, 'web' => $web, 'webCache' => $webCache];
    }

    /**
     * @return string
     */
    public function getCreateDateLabel()
    {
        return AppHelper::date2label($this->date_create);
    }

    /**
     * @return string
     */
    public function getUpdateDateLabel()
    {
        return AppHelper::date2label($this->date_update);
    }

    /**
     * @return string
     */
    public function getDateLabel()
    {
        return Yii::$app->formatter->asDatetime($this->date, 'Y-MM-dd');
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->author ? ($this->author->firstname . ' ' . $this->author->lastname) : '--';
    }

    /**
     * @return string
     */
    public function getPreviewAsImage()
    {
        $paths = self::filePaths('preview');
        return Html::img($paths['web'] . $this->preview);
    }

    /**
     * @return string
     */
    public function getPreviewSmall()
    {
        $pic = '';
        if(!empty($this->preview)) {
            $pws = self::pws('preview', 'grid');
            $paths = self::filePaths('preview');
            if(is_array($pws)) {
                try {
                    $pic = Html::a(Html::img(ImagePreview::p($this->preview, $pws, $paths)),
                        $paths['web'] . $this->preview, ['rel' => 'fancybox']);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), 'PREVIEW');
                }
            }
        }
        return $pic;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function rangeDateYear()
    {
        static $data;
        if(!is_null($data))
            return $data;
        /* @var $data [] */
        $data = Yii::$app->db->cache(function ($db) {
            return (new Query())->select(['min(year(date)) as min', 'max(year(date)) as max'])
                ->from(self::tableName())->one();
        }, AppHelper::CACHE_TIMEOUT, AppHelper::DbDependency(Books::tableName(), 'date_update'));
        if(!count($data))
            $data = ['min' => date('Y'), 'max' => date('Y')];
        return $data;
    }
}
