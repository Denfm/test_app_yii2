<?php
namespace app\modules\books;

use yii\base\BootstrapInterface;
use Yii;
use yii\console\Application as ConsoleApp;
use yii\data\Pagination;
use \yii\web\Application as WebApp;

class Bootstrap implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        // Устанавливаем алиас для путей к модулю
        Yii::setAlias('books', '@app/modules/books');
        /* @var $module Module */
        if($module = Yii::$app->getModule('books')) {
            $moduleId = $module->id;
            if ($app instanceof WebApp) {
                $app->getUrlManager()->addRules([
                    '' => $moduleId . '/base/index', // Начальный экшен
                    'books/update/<id:\d+>' => $moduleId . '/base/update',
                    'books/delete/<id:\d+>' => $moduleId . '/base/delete',
                    'books/create/' => $moduleId . '/base/create',
                    'books/view/<id:\d+>' => $moduleId . '/base/view',
                    'books/search/data' => $moduleId . '/base/filter-search-data',
                    'books' => $moduleId . '/base/index',
                ], false);
                Yii::$container->set(Pagination::className(), [
                    'pageSizeLimit' => [1, 100],
                    'defaultPageSize' => $module->pageSize
                ]);
            } elseif($app instanceof ConsoleApp) {
                $module->controllerNamespace = 'app\modules\books\commands\controllers';
            }
        }
    }
}
