<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'BOOKS_ATTR_ID' => 'ID',
    'BOOKS_ATTR_NAME' => 'Наименование',
    'BOOKS_ATTR_DATE' => 'Дата выпука',
    'BOOKS_ATTR_DATE_CREATE' => 'Дата создания',
    'BOOKS_ATTR_DATE_UPDATE' => 'Дата обновления',
    'BOOKS_ATTR_PREVIEW' => 'Постер',
    'BOOKS_ATTR_AUTHOR_ID' => 'Автор',

    'BOOKS_ATTR_AUTHOR_CHOOSE' => ' - Выбрать автора - ',
    'BOOKS_ATTR_DATE_START' => 'Дата выхода книги',
    'BOOKS_ATTR_DATE_END' => 'до',

    'BOOKS_VIEW_BR_INDEX' => 'Библиотека',
    'BOOKS_VIEW_BR_VIEW' => 'Просмотр',
    'BOOKS_VIEW_BR_CREATE' => 'Добавить',
    'BOOKS_VIEW_BR_UPDATE' => 'Редактирование',
    'BOOKS_MODAL_VIEW_LABEL' => 'Быстрый просмотр',

    'BOOKS_BTN_DELETE_CONFIRM' => 'Вы уверены что хотите удалить книгу?'
];
