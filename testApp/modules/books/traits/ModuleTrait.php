<?php
namespace app\modules\books\traits;
use app\modules\books\Module;

trait ModuleTrait
{
    private $_module;

    /**
     * @return null|Module
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Module::getInstance();

            if(!$this->_module)
                $this->_module = \Yii::$app->getModule('books');
        }
        return $this->_module;
    }
}
