<?php
namespace app\modules\books\commands\controllers;

use app\modules\books\models\Authors;
use app\modules\books\models\Books;
use app\modules\books\traits\ModuleTrait;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;


/**
 * Default backend controller.
 * @property \app\modules\books\Module $module
 */

class ConsoleController extends Controller
{
    use ModuleTrait;

    /**
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionInstall()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $this->actionUninstall();
            $path = Yii::getAlias('@books') . DIRECTORY_SEPARATOR . 'commands' . DIRECTORY_SEPARATOR . 'data' .
                DIRECTORY_SEPARATOR;
            foreach(['authors', 'books'] as $fileName) {
                $cSQL = file_get_contents($path . $fileName . '.sql');
                Yii::$app->db->createCommand($cSQL)->execute();
            }
            $copy2path =  Books::filePaths('preview', true)['root'];
            foreach(FileHelper::findFiles($path . 'media', ['only' => ["*.jpg"]]) as $file)
                copy($file, $copy2path . pathinfo($file, PATHINFO_BASENAME));
            $assetsPath = realpath(Yii::getAlias('@uploads_root') . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR .
                'assets' . DIRECTORY_SEPARATOR;
            FileHelper::createDirectory($assetsPath);
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionUninstall()
    {
        $connection = Yii::$app->db;
        foreach(Authors::find()->all() as $model)
            $model->delete();
        $connection->createCommand('alter table ' . Authors::tableName() . ' AUTO_INCREMENT = 1')->execute();
        foreach (Books::find()->all() as $model)
            $model->delete();
        $connection->createCommand('alter table ' . Books::tableName() . ' AUTO_INCREMENT = 1')->execute();
        $assetsPath = realpath(Yii::getAlias('@uploads_root') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .
            'assets' . DIRECTORY_SEPARATOR);
        if($assetsPath)
            FileHelper::removeDirectory($assetsPath);
        FileHelper::removeDirectory(Books::filePaths('preview', false)['root']);
        FileHelper::removeDirectory(Yii::$app->runtimePath);
    }
}
