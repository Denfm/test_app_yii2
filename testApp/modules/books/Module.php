<?php

namespace app\modules\books;
use Yii;

class Module extends \yii\base\Module
{
    public $defaultRoute = 'base/';
    public $controllerNamespace = 'app\modules\books\controllers';
    public $pageSize = 10;

    public function init()
    {
        parent::init();
        $this->setViewPath('@books/views');
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['app/books/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@books/messages',
            'fileMap' => ['app/books/main' => 'main.php'],
        ];
    }

    /**
     * @param $message
     * @param string $category
     * @param array $params
     * @param null $language
     * @return string
     */
    public static function t($message, $category = 'main', $params = [], $language = null)
    {
        return Yii::t('app/books/' . $category, $message, $params, $language);
    }
}
