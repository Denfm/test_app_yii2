<?php
namespace app\modules\books\controllers;

use app\components\AppHelper;
use app\modules\books\models\Authors;
use app\modules\books\models\Books;
use app\modules\books\models\search\BooksSearch;
use app\modules\books\models\form\BooksUserFilter;
use app\modules\books\traits\ModuleTrait;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Controller;
use app\modules\books\Module;
use yii\web\UploadedFile;


/**
 * Default backend controller.
 * @property \app\modules\books\Module $module
 */

class BaseController extends Controller
{
    use ModuleTrait;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = ['access' => ['class' => AccessControl::className(), 'rules' => [
            ['allow' => true, 'actions' => ['index', 'filter-search-data', 'view'], 'roles' => ['BooksList']],
            ['allow' => true, 'actions' => ['create'], 'roles' => ['BooksCreate']],
            ['allow' => true, 'actions' => ['update'], 'roles' => ['BooksUpdate']],
            ['allow' => true, 'actions' => ['delete'], 'roles' => ['BooksDelete']],
        ]]];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['get'],
                'view' => ['get'],
                'create' => ['get', 'post'],
                'update' => ['get', 'put', 'post'],
                'delete' => ['post', 'delete'],
            ]
        ];

        return $behaviors;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->view->title = Module::t('BOOKS_VIEW_BR_INDEX');
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $filterForm = new BooksUserFilter();
        $search = new BooksSearch();
        $params = [];
        $author_label = '';
        if($filterForm->load(Yii::$app->request->get()) && $filterForm->validate())
            $params = $filterForm->getParams();

        if(isset($params['author_id'])) {
            try {
                $author_label = $this->findModelAuthor($params['author_id'])->fullName;
            } catch(HttpException $e) {}
        }

        return $this->render('index', [
            'author_label' => $author_label,
            'filterForm' => $filterForm,
            'dataProvider' => $search->search($params)
        ]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Books();
        $stateIndex = $this->stateAction(['index'], 'create');
        if($model->load(Yii::$app->request->post())) {
            $model->preview = UploadedFile::getInstance($model, 'preview');
            if($model->validate()) {
                $this->saveFile($model, 'preview');
                $model->save(false);
                $this->redirect($stateIndex ? $stateIndex : ['index']);
            }
        }
        $this->view->title = Module::t('BOOKS_VIEW_BR_CREATE');
        $this->view->params['breadcrumbs'][] = ['label' => Module::t('BOOKS_VIEW_BR_INDEX'), 'url' => ['index']];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        return $this->render('form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelBooks($id);
        $stateIndex = $this->stateAction(['index'], $this->action->id . $id);
        if($model->load(Yii::$app->request->post())) {
            $model->preview = UploadedFile::getInstance($model, 'preview');
            if($model->validate()) {
                $this->saveFile($model, 'preview');
                $model->save(false);
                $this->redirect($stateIndex ? $stateIndex : ['index']);
            }
        }
        $this->view->title = $model->name;
        $this->view->params['breadcrumbs'][] = ['label' => Module::t('BOOKS_VIEW_BR_INDEX'), 'url' => ['index']];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        return $this->render('form', ['model' => $model]);
    }

    /**
     * @param ActiveRecord $model
     * @param $attribute
     */
    protected function saveFile(ActiveRecord &$model, $attribute)
    {
        if($model->{$attribute} instanceof UploadedFile) {
            /* @var $instance UploadedFile */
            $instance = $model->{$attribute};
            $file = Yii::$app->security->generateRandomString(15) . '.' . $instance->extension;
            $paths = $model->filePaths($attribute);
            if(isset($model->oldAttributes[$attribute]) && !empty($model->oldAttributes[$attribute]))
                AppHelper::removeFileWithCache($paths, $model->oldAttributes[$attribute]);
            $instance->saveAs($paths['root'] .$file);
            $model->{$attribute} = $file;
        } else
            $model->{$attribute} = $model->oldAttributes[$attribute];
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModelBooks($id);
        if(Yii::$app->request->isAjax)
            return $this->renderAjax('view', ['model' => $model]);
        $this->view->title = $model->name;
        $this->view->params['breadcrumbs'][] = ['label' => Module::t('BOOKS_VIEW_BR_INDEX'), 'url' => ['index']];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        return $this->render('view', ['model' => $model]);
    }

    /**
     * @param $id
     * @throws HttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModelBooks($id);
        $model->delete();
    }

    /**
     * @param $model
     * @param null $q
     * @param int $limit
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionFilterSearchData($model, $q = null, $limit = 20)
    {
        if(!in_array($model, ['authors']))
            throw new BadRequestHttpException(Yii::t('app', 'Некорректный запрос.'));

        if($model == 'authors')
            $attributes = ['firstname', 'lastname'];
        else
            $attributes = ['name'];

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $modelName ActiveRecord */
        $modelName = 'app\modules\books\models\\' . ucfirst($model);
        $out = ['results' => ['id' => '', 'text' => '']];
        if($q && mb_strlen($q) > 2) {
            $data = [];
            $query = $modelName::find();
            foreach($attributes as $attribute)
                $query->orFilterWhere(['like', (string)$attribute,  $q]);
            $models = $query->limit((int)$limit)->all();
            foreach($models as $model) {
                $label = [];
                foreach($attributes as $attribute)
                    $label[] = $model->{$attribute};
                $data[] = ['id' => $model->id, 'text' => implode(' ', $label)];
            }
            $out['results'] = array_values($data);
        }
        return $out;
    }

    /**
     * @param array $route
     * @param $state_key
     * @return bool|mixed
     */
    protected function stateAction(Array $route, $state_key)
    {
        $referrer = Yii::$app->request->referrer;
        $route = Url::toRoute($route, true);
        $session = Yii::$app->session;

        // Похоже, что зашли по прямой ссылке на экшен
        if(empty($referrer))
            $session->remove($state_key);
        else {
            $parse_referrer = parse_url($referrer);
            $url = $parse_referrer['scheme'] . '://';
            $url .= $parse_referrer['host'];
            if(isset($parse_referrer['path']))
                $url .= $parse_referrer['path'];
            // Зашли откуда нужно, пишем стейт
            if($url == $route)
                $session->set($state_key, $referrer);
        }

        // Если есть стейт для данного экшена, то отдаем его
        return $session->has($state_key) ? $session->get($state_key) : false;
    }

    /**
     * @param $id
     * @return Authors
     * @throws HttpException
     */
    protected function findModelAuthor($id)
    {
        /* @var $model Authors|Null */
        $model = Yii::$app->db->cache(function ($db) use ($id) {
            return (is_array($id)) ? Authors::findAll($id) : Authors::findOne($id);
        }, AppHelper::CACHE_TIMEOUT, AppHelper::DbDependency(Authors::tableName(), 'id'));

        if(!$model)
            throw new HttpException(404, 'Запись не найдена');

        return $model;
    }

    /**
     * @param $id
     * @return Books
     * @throws HttpException
     */
    protected function findModelBooks($id)
    {
        /* @var $model Books|Null */
        $model = Yii::$app->db->cache(function ($db) use ($id) {
            return (is_array($id)) ? Books::findAll($id) : Books::findOne($id);
        }, AppHelper::CACHE_TIMEOUT, AppHelper::DbDependency(Books::tableName(), 'date_update'));

        if(!$model)
            throw new HttpException(404, 'Запись не найдена');

        return $model;
    }
}
