<?php
/**
 * @var yii\base\View $this View
 * @var \app\modules\books\models\Books $model Model
 */

use \yii\bootstrap\Html;
use kartik\select2\Select2;
use \yii\helpers\Url;
use app\modules\books\Module as ModuleBooks;
use \yii\web\JsExpression;
?>

<?php $form = \yii\bootstrap\ActiveForm::begin(['method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);?>
<div class="col-xs-12">
    <h2><?php echo $this->title?></h2>
    <div class="row row-bg">
        <div class="col-xs-4">
            <?php echo $form->field($model, 'name')->textInput()?>
        </div>
        <div class="col-xs-4">
            <?php echo $form->field($model, 'author_id')->widget(Select2::className(), [
                'initValueText' => $model->authorName,
                'options' => ['placeholder' => ModuleBooks::t('BOOKS_ATTR_AUTHOR_CHOOSE'), 'id' => 'author_id_sel'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::toRoute(['/books/base/filter-search-data', 'model' => 'authors']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {return{q:params.term};}')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) {return markup;}'),
                    'templateResult' => new JsExpression('function(data) {return data.text;}'),
                    'templateSelection' => new JsExpression('function (data) {return data.text;}'),
                ]]); ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-4">
            <?php echo $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), ['language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd', 'options' => ['class' => 'form-control'], 'clientOptions' =>
                    ['changeMonth' => true, 'changeYear' => true,]]);?>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-4">
            <div>
                <?php echo $model->previewAsImage?>
            </div>
            <?php echo $form->field($model, 'preview')->fileInput(['accept' => 'image/*']);?>
        </div>
        <div class="clearfix"></div>
        <div class="sbtBox">
            <?php echo \yii\bootstrap\Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary'])?>
        </div>
    </div>
</div>
<?php $form->end()?>