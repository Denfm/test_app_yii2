<?php
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $filterForm app\modules\books\models\form\BooksUserFilter */
/* @var $author_label string */
use \yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\web\JsExpression;
use \yii\helpers\Url;
use  \yii\grid\GridView;
use kartik\select2\Select2;
use app\modules\books\Module as ModuleBooks;
use app\modules\books\models\Books;
?>
<?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['/books/base/index']]) ?>
<div class="boxFilters">
    <div class="col">
        <?php echo $form->field($filterForm, 'name')->textInput(['class' => 'form-control input-sm'])?>
    </div>
    <div class="col">
        <?php echo Html::label($filterForm->getAttributeLabel('author_id'), 'author_id_sel')?>
        <?php echo $form->field($filterForm, 'author_id')->widget(Select2::className(), [
            'initValueText' => $author_label,
            'options' => ['placeholder' => ModuleBooks::t('BOOKS_ATTR_AUTHOR_CHOOSE'), 'id' => 'author_id_sel'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Url::toRoute(['/books/base/filter-search-data', 'model' => 'authors']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) {return{q:params.term};}')
                ],
                'escapeMarkup' => new JsExpression('function (markup) {return markup;}'),
                'templateResult' => new JsExpression('function(data) {return data.text;}'),
                'templateSelection' => new JsExpression('function (data) {return data.text;}'),
            ],
            'size' => Select2::SMALL])->label(false); ?>
    </div>
    <div class="col">
        <?php echo $form->field($filterForm, 'date_start')->widget(\yii\jui\DatePicker::classname(), ['language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd', 'options' => ['class' => 'form-control input-sm'], 'clientOptions' =>
                ['changeMonth' => true, 'changeYear' => true, 'yearRange' =>
                    Books::rangeDateYear()['min'] . ':' . Books::rangeDateYear()['max']]]);?>
    </div>
    <div class="col">
        <?php echo $form->field($filterForm, 'date_end')->widget(\yii\jui\DatePicker::classname(), ['language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd', 'options' => ['class' => 'form-control input-sm'], 'clientOptions' =>
                ['changeMonth' => true, 'changeYear' => true, 'yearRange' =>
                    Books::rangeDateYear()['min'] . ':' . Books::rangeDateYear()['max']]]);?>
    </div>
    <div class="col-btn">
        <?php echo Html::submitButton(Yii::t('app', 'Применить фильтр'), ['class' => 'btn btn-primary'])?>
    </div>
    <div style="clear: both"></div>
</div>
<?php $form->end()?>
<div class="col-xs-12">
    <?php
    echo newerton\fancybox\FancyBox::widget(['target' => 'a[rel=fancybox]', 'helpers' => true,
        'mouse' => true, 'config' => []]);?>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php echo GridView::widget([
        'id' => 'BooksGrid',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['attribute' => 'id', 'format' => 'raw', 'value' => function($data) {return $data->id;},
                'options' => ['width' => '70']],
            ['attribute' => 'name', 'format' => 'raw', 'value' => function($data) {return $data->name;}],
            ['attribute' => 'preview', 'format' => 'raw', 'value' => function($data) {
                return $data->previewSmall;
            }, 'options' => ['width' => '140']],
            ['attribute' => 'author_id', 'format' => 'raw', 'value' => function($data) {return $data->authorName;}],
            ['attribute' => 'date', 'format' => 'raw', 'value' => function($data) {return $data->dateLabel;}],
            ['attribute' => 'date_create', 'format' => 'raw', 'value' => function($data) {return $data->createDateLabel;}],
            ['header' => '', 'format' => 'raw', 'value' => function($data) {
                $build = [Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']),'#', [
                    'data' => [
                        'toggle' => 'modal',
                        'target' => 'booksModal',
                        'url' => Url::toRoute(['/books/base/view', 'id' => $data->id])
                    ],
                    'class' => 'booksModalEventBtn',
                    'title' => ModuleBooks::t('BOOKS_MODAL_VIEW_LABEL'),
                ])];
                $build[] = Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
                    ['/books/base/update', 'id' => $data->id], ['data-pjax' => 'false']);
                $build[] = Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
                    ['/books/base/delete', 'id' => $data->id], ['class' => 'booksDeleteRow', 'data-pjax' => 'false']);
                return implode("\r\n", $build);
            }]
        ],
    ])?>
    <?php $this->registerJs("
        $('.booksDeleteRow').click(function() {
            if(confirm('" . ModuleBooks::t('BOOKS_BTN_DELETE_CONFIRM') . "')) {
                $.ajax({
                    type: 'post',
                    url:$(this).attr('href'),
                    data: yii.getCsrfParam() + '=' + yii.getCsrfToken(),
                    success: function(data) {\$.pjax.reload({container: '#BooksGrid'});}
                });
            }
            return false;
        });
        $('.booksModalEventBtn').click(function() {
            $.get($(this).data('url'),{},
            function (data) {
                $('.modal-body').html(data);
                $('#booksModal').modal();
            });
        });
    ");?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
<?php \yii\bootstrap\Modal::begin([
    'id' => 'booksModal',
    'header' => Html::tag('h3', ModuleBooks::t('BOOKS_MODAL_VIEW_LABEL'), ['class' => 'modal-title']),
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',
]); ?>
