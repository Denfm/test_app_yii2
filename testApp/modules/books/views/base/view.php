<?php /* @var $model \app\modules\books\models\Books */ ?>
<table class="table table-bordered table-striped">
    <tr>
        <td rowspan="8">
            <div class="view_max_image">
                <?php echo $model->previewAsImage?>
            </div>
        </td>
    </tr>
    <tr>
        <th><?php echo $model->getAttributeLabel('id')?></th>
        <td><?php echo $model->id?></td>
    </tr>
    <tr>
        <th><?php echo $model->getAttributeLabel('name')?></th>
        <td><?php echo $model->name?></td>
    </tr>
    <tr>
        <th><?php echo $model->getAttributeLabel('author_id')?></th>
        <td><?php echo $model->authorName?></td>
    </tr>
    <tr>
        <th><?php echo $model->getAttributeLabel('date')?></th>
        <td><?php echo $model->dateLabel?></td>
    </tr>
    <tr>
        <th><?php echo $model->getAttributeLabel('date_create')?></th>
        <td><?php echo $model->createDateLabel?></td>
    </tr>
    <tr>
        <th><?php echo $model->getAttributeLabel('date_update')?></th>
        <td><?php echo $model->updateDateLabel?></td>
    </tr>
</table>
