<?php
namespace app\modules\users\models\form;
use app\modules\users\models\Users;
use yii\base\Model;
use app\modules\users\Module;

class LoginForm extends Model
{
    /**
     * @var string $username Username
     */
    public $username;

    /**
     * @var string $password Password
     */
    public $password;
    /**
     * @var boolean rememberMe Remember me
     */
    public $rememberMe = true;

    /**
     * @var Users|null User instance
     */
    private $_user;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['password', 'validatePassword'],
            ['rememberMe', 'boolean']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => Module::t('USERS_ATTR_USERNAME'),
            'password' => Module::t('USERS_ATTR_PASSWORD'),
            'rememberMe' => Module::t('USERS_FORM_SAVE_PASSWORD')
        ];
    }

    /**
     * @param $attribute
     */
    public function validatePassword($attribute)
    {
        $user = $this->getUser();
        if (!$user || !$user->validatePassword($this->$attribute)) {
            $this->addError($attribute, Module::t('USERS_FORM_INCORRECT'));
        }
    }
    /**
     * Finds user by username.
     *
     * @return Users|null User instance
     */
    protected function getUser()
    {
        if ($this->_user === null)
            $this->_user = Users::findByUsername($this->username);
        return $this->_user;
    }
    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        return \Yii::$app->user->login($this->user, $this->rememberMe ? Module::$loginRememberTime : 0);
    }
}