<?php
namespace app\modules\users\models;
use app\modules\users\traits\ModuleTrait;
use app\modules\users\Module;
use Yii;
use yii\base\Object;
use yii\web\IdentityInterface;

class Users extends Object implements IdentityInterface
{
    use ModuleTrait;

    public $id;
    public $role;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        1 => [
            'id' => 1,
            'role' => 'user',
            'username' => 'den',
            'password' => 'BJtWU',
            'authKey' => 'BJtWUldQkqViqYPubAeaRj0BCeAEVuEp',
            'accessToken' => 'wHTqZPCKAE6cvGvqdZPxlufGztkzBSsg',
        ],
    ];

    /**
     * @param int|string $id
     * @return null|static
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @param $username
     * @param $password
     * @return null|static
     */
    public static function findByBasicAuth($username, $password)
    {
        foreach(self::$users as $user) {
            if($user['username'] == $username && $user['password'] == $password)
                return new static($user);
        }
        return null;
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return null|static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function getAttributeLabel($attribute)
    {
        return Module::t('USERS_ATTR_' . strtoupper($attribute));
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
