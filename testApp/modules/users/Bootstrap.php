<?php
namespace app\modules\users;

use yii\base\BootstrapInterface;
use Yii;
use \yii\web\Application as WebApp;

class Bootstrap implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        // Устанавливаем алиас для путей к модулю
        Yii::setAlias('users', '@app/modules/users');
        Yii::setAlias('rbac', '@users/rbac');

        /* @var $module Module */
        if ($app instanceof WebApp && $module = Yii::$app->getModule('users')) {
            $moduleId = $module->id;
            $app->getUrlManager()->addRules([
                'profile/login' => $moduleId . '/guest/login',
                'profile/logout' => $moduleId . '/guest/logout',
            ], false);
        }
    }
}
