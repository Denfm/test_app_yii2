<?php
return [
    'BooksCreate' => [
        'type' => 2,
        'description' => 'Права на создание сущностей "Books" в модуле "Books"',
    ],
    'BooksUpdate' => [
        'type' => 2,
        'description' => 'Права на обновление сущностей "Books" в модуле "Books"',
    ],
    'BooksDelete' => [
        'type' => 2,
        'description' => 'Права на удалание сущностей "Books" в модуле "Books"',
    ],
    'BooksList' => [
        'type' => 2,
        'description' => 'Права на просмотр сущностей "Books" в модуле "Books"',
    ],
    'accessFullBooks' => [
        'type' => 2,
        'description' => 'Полные права на сущности "Books" в модуле "Books"',
        'children' => [
            'BooksCreate',
            'BooksUpdate',
            'BooksDelete',
            'BooksList'
        ],
    ],
    'user' => [
        'type' => 1,
        'description' => 'Пользователь системы',
        'children' => [
            'accessFullBooks',
        ],
    ],
    'guest' => [
        'type' => 1,
        'description' => 'Гость',
    ]
];
