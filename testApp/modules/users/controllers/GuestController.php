<?php
namespace app\modules\users\controllers;

use app\modules\books\traits\ModuleTrait;
use app\modules\users\models\form\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\modules\users\Module;


/**
 * Default backend controller.
 * @property \app\modules\users\Module $module
 */

class GuestController extends Controller
{
    use ModuleTrait;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout'],
                'rules' => [
                    ['allow' => true, 'actions' => ['login'], 'roles' => ['?'],],
                    ['allow' => true, 'actions' => ['logout'], 'roles' => ['@']],
                ],
                'denyCallback' => function ($rule, $action) {
                    if($action->id == 'login')
                        $this->goHome();
                    else
                        $this->redirect(['login']);
                }
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'login' => ['get', 'post'],
                'logout' => ['get'],
            ]
        ];

        return $behaviors;
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        $formModel = new LoginForm();
        if ($formModel->load(Yii::$app->request->post())) {
            if ($formModel->validate()) {
                if($formModel->login())
                    $this->goHome();
            }
        }

        $this->view->title = Module::t('USERS_VIEW_BR_LOGIN');
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('login', [
            'formModel' => $formModel
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
