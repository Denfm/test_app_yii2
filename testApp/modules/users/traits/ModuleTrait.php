<?php
namespace app\modules\users\traits;
use app\modules\users\Module;

trait ModuleTrait
{
    private $_module;

    /**
     * @return null|Module
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Module::getInstance();

            if(!$this->_module)
                $this->_module = \Yii::$app->getModule('users');
        }
        return $this->_module;
    }
}
