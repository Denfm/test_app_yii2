<?php

namespace app\modules\users;
use Yii;

class Module extends \yii\base\Module
{
    public static $loginRememberTime = 2592000; // 30 дней
    public $defaultRoute = 'guest/';
    public $controllerNamespace = 'app\modules\users\controllers';

    public function init()
    {
        parent::init();
        $this->setViewPath('@users/views');
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['app/users/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@users/messages',
            'fileMap' => ['app/users/main' => 'main.php'],
        ];
    }

    /**
     * @param $message
     * @param string $category
     * @param array $params
     * @param null $language
     * @return string
     */
    public static function t($message, $category = 'main', $params = [], $language = null)
    {
        return Yii::t('app/users/' . $category, $message, $params, $language);
    }
}
