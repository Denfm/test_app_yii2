<?php /* @var $formModel \app\modules\users\models\form\LoginForm */ ?>
<?php use app\modules\users\Module;?>
<?php use yii\bootstrap\Html;?>
<?php use yii\bootstrap\ActiveForm;?>
<div class="col-xs-12">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <div class="col-xs-6">
        <h1><?php echo $this->title?></h1>
        <?php echo $form->field($formModel, 'username') ?>
        <?php echo $form->field($formModel, 'password')->passwordInput() ?>
        <?php echo $form->field($formModel, 'rememberMe')->checkbox() ?>
        <?php echo Html::submitButton(Module::t('USERS_FORM_LOGIN_SUBMIT'), ['class' => 'btn btn-lg'])?>
    </div>
</div>
