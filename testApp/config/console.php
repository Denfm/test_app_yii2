<?php
return [
    'id' => 'Console',
    'language' => 'ru-RU',
    'vendorPath' => $vendorPath,
    'timeZone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\commands',
    'bootstrap' => ['log', 'app\modules\books\Bootstrap'],
    'defaultRoute' => 'books/base/index',
    'modules' => [
        'books' => ['class' => 'app\modules\books\Module'],
    ],
    'components' => [
        'cache' => ['class' => 'yii\caching\FileCache', 'keyPrefix' => 'console', 'cachePath' => '@runtime/cache/console'],
        'db' => require(__DIR__ . '/db.php'),
        'formatter' => ['locale' => 'ru', 'dateFormat' => 'dd.MM.y', 'datetimeFormat' => 'HH:mm:ss dd.MM.y'],
        'log' => [
            'targets' => [['class' => 'yii\log\FileTarget', 'levels' => ['error', 'warning']]],
        ],
        'urlManager' => [
            'baseUrl' => 'http://test_rgkgroup.local/'
        ]
    ],
];
