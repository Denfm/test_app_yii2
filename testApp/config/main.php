<?php

return [
    'id' => 'WebApp',
    'language' => 'ru-RU',
    'vendorPath' => $vendorPath,
    'timeZone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\modules\users\Bootstrap', 'app\modules\books\Bootstrap'],
    'defaultRoute' => 'books/base/index',
    'modules' => [
        'users' => ['class' => 'app\modules\users\Module'],
        'books' => ['class' => 'app\modules\books\Module'],
    ],
    'components' => [
        'request' => ['cookieValidationKey' => 'ZRbsnDSRZGJ2yJyyuLqApQUNGintrHtI'],
        'cache' => ['class' => 'yii\caching\FileCache'],
        'db' => require(__DIR__ . '/db.php'),
        'user' => [
            'identityClass' => 'app\modules\users\models\Users',
            'enableAutoLogin' => true,
            'loginUrl' => ['/users/guest/login'],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'itemFile' => '@rbac/data/items.php',
            'assignmentFile' => '@rbac/data/assignments.php'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' => []
        ],
        'view' => [
            'theme'    => [
                'class' => 'app\themes\dashboard\Theme',
                'basePath' => '@app/',
                'baseUrl'  => '@web/',
            ]
        ],
        'formatter' => [
            'locale' => 'ru',
            'dateFormat' => 'dd.MM.y',
            'datetimeFormat' => 'HH:mm:ss dd.MM.y'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                ['class' => 'yii\log\FileTarget', 'levels' => ['error', 'warning']]
            ]
        ]
    ]
];
