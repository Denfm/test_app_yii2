<?php
Yii::setAlias('uploads', 'uploads');
// Для веба и консольного приложения устанавливаем полный путь от этого файла
Yii::setAlias('uploads_root',  realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'htdocs') . DIRECTORY_SEPARATOR . Yii::getAlias('@uploads'));
