<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=db',
    'username' => 'user',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix' => 'c_'
];